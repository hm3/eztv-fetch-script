~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This script is based on https://github.com/PaulSec/eztv_api
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Before you run the script make sure you installed: 
- beautifulsoup4
- requests

How to use the script: 

```python                                                                                                                                               
python get_show.py 'SERIES NAME' INT(SEASON NUMBER)
                                                                 
                                                                                                                        
```

1. Running command above will fetch series and season (only one number per call).
2. If the number of the season is not set, everything will be fetched.
3. Script does not do a links filtering, thus you will get both HD or HDTV etc.
4. In order to save the results just call:


```python
python get_show.py 'SERIES NAME' INT(SEASON NUMBER) > SERIES.txt                                                                                                     
                                                      

                                                                                        
```

This is really simple implementation so do not expect miracles ;) 

from eztv_api import EztvAPI
import sys
import urllib2


if len(sys.argv) == 1:
    
   print "Name of the show not given\nGive in the quotes: eg. 'Company of Brothers'"

else: 
    
     if len(sys.argv) > 3 :
    
        print "Name: "+sys.argv[1] + " - season: [" +sys.argv[2] + "] episode: [" +sys.argv[3]+"]"
     #set the api
        test_api = EztvAPI().tv_show(sys.argv[1])
        seasons = test_api.seasons()
    
        season =  int(sys.argv[2])
        episode =  int(sys.argv[3])
    
        print test_api.episode(season, episode)
        
     else:
         
         print "Invalid data! Stopped."
    

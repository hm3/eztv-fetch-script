from eztv_api import EztvAPI
import sys
import urllib2



if len(sys.argv) == 1:
    
   print "Name of the show not given\nGive in the quotes: eg. 'Company of Brothers'"

else: 
    
    #get file setter
        
    print "Title set: "+sys.argv[1]
    
    #set the api
    test_api = EztvAPI().tv_show(sys.argv[1])
    seasons = test_api.seasons()
    
    if len(sys.argv) > 2 :
       
        season_no = int(sys.argv[2])
        print "Season number: "+sys.argv[2]+" to fetch."
        episodes = test_api.season(season_no)
        for episode in episodes:
    # will print the magnet link for all episodes
            print episodes[episode] 
            
    else:
        
        print "nothing found fetch all\nFetching all..."
        for season in seasons:
            for episode in seasons[season]:
        # will print the magnet link of all episodes, in all seasons
                print seasons[season][episode]
               
                
               



